/**
 * Created by Victor on 2017/5/17.
 * QQ:1046512080
 */

/*Validator类*/
class Validator {
    constructor() {
        this.cache = [] //保存校验规则
    }
    /**
     * 添加验证
     * @param name      备注名称
     * @param str       待验证的字符串
     * @param rules     验证规则
     * @param dom       原样返回,可以操作dom
     * @returns {*}
     */
    add(name = '',str = '', rules,dom = '') {
        if(typeof rules === 'undefined') return false;
        if (rules.length === 0) return false;
        return this.cache.push([name,str, rules,dom]);
    }

    /**
     * 开始验证
     * 验证失败返回 {error:1,info:'....',dom:''};
     * 验证成功返回 {error:0,info:'验证通过'};
     * @returns {*}
     */
    start() {
        let rules = this.cache;
        for (let i in rules){
            let _name = rules[i][0];        //备注名称
            let _str = rules[i][1];         //待验证的字符串
            let _dom = rules[i][3];         //原样返回,可以操作dom
            for (let j in rules[i][2]){
                //以冒号为界
                if(rules[i][2][j].indexOf(':') > 0){
                    let ruleArr = rules[i][2][j].split(':');
                    let _rule = ruleArr[0].toLocaleLowerCase();
                    let _value = ruleArr[1];
                    switch (_rule){
                        case 'minlength':
                            if (_str.length < parseInt(_value)){
                                return this.error(_name + '最小长度不能小于' + _value,_dom)
                            }
                            break;
                        case 'maxlength':
                            if (_str.length > parseInt(_value)){
                                return this.error(_name + '最大长度不能大于' + _value,_dom)
                            }
                            break;
                    }

                }else{
                    //不存在冒号
                    let _rule = rules[i][2][j].toLocaleLowerCase();
                    switch (_rule){
                        //不能为空
                        case 'require':
                            if (_str === ''){
                                return this.error(_name + '不能为空',_dom)
                            }
                            break;
                        //验证邮箱格式
                        case 'email':
                            let re_email = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
                            if(!re_email.test(_str)){
                                return this.error(_name + '格式不合法',_dom);
                            }
                            break;
                        //验证URL
                        case 'url':
                            let re_url = /^[a-zA-z]+:\/\/[^\s]*$/;
                            if (!re_url.test(_str)){
                                return this.error(_name + '格式不合法',_dom)
                            }
                            break;
                        //验证手机号
                        case 'phone':
                            let re_phone = /^1(3|4|5|7|8)\d{9}$/;
                            if (!re_phone.test(_str)){
                                return this.error(_name + '格式不合法',_dom)
                            }
                            break;
                        //验证IP
                        case 'ip':
                            let re_ip = /^\d+\.\d+\.\d+\.\d+$/;
                            if (!re_ip.test(_str)){
                                return this.error(_name + '格式不合法',_dom)
                            }
                            break;
                        //验证中文
                        case 'cn':
                            let re_cn = /^[\u4e00-\u9fa5]+$/;
                            if (!re_cn.test(_str)){
                                return this.error(_name + '格式不合法',_dom)
                            }
                            break;

                        default:
                            return {error:1,info:'不存在的验证规则'};

                    }
                }
            }
        }
        return {error:0,info:'验证通过'};
    }

    error(info,dom){
        return {error:1,info:info,dom:dom}
    }

}